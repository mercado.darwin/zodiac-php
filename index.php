<!DOCTYPE html>
<html>
<head>
	<title>Zodiac</title>
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/superhero/bootstrap.css">
</head>
<body class="d-flex justify-content-center align-items-center vh-100">
	<div>
		<h1>Welcome</h1>
		<div>
			<form class="bg-light" action="controllers/controller.php" method="POST">
				<div class="form-group p-1">
					<label class="text-secondary">First Name</label>
					<input type="text" name="firstName" class="form-control">
				</div>
				<div class="form-group p-1">
					<label class="text-secondary">Last Name: </label>
					<input type="text" name="lastName" class="form-control">
				</div>
				<div class="form-group p-1">
					<label class="text-secondary">Age: </label>
					<input type="number" name="age" class="form-control">
				</div>
				<div class="form-group p-1">
					<label class="text-secondary">Birth Month: </label>
					<input type="text" name="birthMonth" class="form-control">
				</div>
				<div class="form-group p-1">
					<label class="text-secondary">Birth date: </label>
					<input type="number" name="birthDate" class="form-control">
				</div>
				<div class="text-center">
					<button type="submit" class="btn btn-success">Register</button>
				</div>
				
			</form>
		</div>
	</div>

	

</body>
</html>