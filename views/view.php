<!DOCTYPE html>
<html>
<head>
	<title>Landing Page</title>
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/superhero/bootstrap.css">
</head>
<body>
	<div class="d-flex justify-content-center flex-column align-items-center vh-100">
		<?php
			session_start();
			
		?>

		<h1>Hello <?php echo $_SESSION['firstName'], " ", $_SESSION['lastName']?></h1><br>
		<h3>Your Zodiac Sign is: <?php echo $_SESSION['zodiac']?></h3><br>
		<h3>Your Age is: <?php echo $_SESSION['age']?></h3><br>
		<h3>Your Birthday is: <?php echo $_SESSION['birthMonth'], " ",$_SESSION['birthDate'], ", ", $_SESSION['yearBorn']?></h3><br>

		
	</div>

</body>
</html>